package eu.hydrologis.hyuml;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import eu.hydrologis.hyuml.ImageUtils.Diagram;

@SuppressWarnings("nls")
public class HyumlActivity extends Activity {

    private static final int MENU_ABOUT = Menu.FIRST;
    private static final int MENU_EXIT = 2;
    private static final int MENU_SAVE = 3;
    private static final int MENU_SAVEAS = 4;
    private static final int MENU_SAVEIMAGE = 5;
    private static final int MENU_LOAD = 6;

    private Spinner typeSpinner;

    private Diagram currentType = Diagram.CLASS;

    private EditText scriptText;

    private ImageView imageView;
    private Bitmap currentImage;
    private File hyumlDir;
    private boolean canSave;
    private Spinner orientSpinner;
    private String currentOrientation;
    private Spinner scaleSpinner;
    private String currentScale;

    /** Called when the activity is first created. */
    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        imageView = (ImageView) findViewById(R.id.imageView);
        scriptText = (EditText) findViewById(R.id.scriptfield);

        Button renderButton = (Button) findViewById(R.id.renderButton);
        renderButton.setOnClickListener(new Button.OnClickListener(){

            public void onClick( View v ) {
                String script = scriptText.getText().toString();
                try {
                    Bitmap newImage = ImageUtils.getImage(script, currentType, currentOrientation, currentScale);
                    imageView.setImageBitmap(newImage);

                    if (currentImage != null) {
                        currentImage.recycle();
                    }
                    currentImage = newImage;
                } catch (Exception e) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(HyumlActivity.this);
                    builder.setMessage("An error occurred while generating the diagram.").setCancelable(false)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
                                public void onClick( DialogInterface dialog, int id ) {
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                    e.printStackTrace();
                }
            }
        });

        typeSpinner = (Spinner) findViewById(R.id.typeSpinner);
        ArrayAdapter< ? > typeSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.array_types,
                android.R.layout.simple_spinner_item);
        typeSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(typeSpinnerAdapter);
        typeSpinner.setSelection(Diagram.CLASS.getIndex());
        currentType = Diagram.CLASS;
        typeSpinner.setOnItemSelectedListener(new OnItemSelectedListener(){
            public void onItemSelected( AdapterView< ? > arg0, View arg1, int arg2, long arg3 ) {
                Object selectedItem = typeSpinner.getSelectedItem();
                currentType = Diagram.fromName(selectedItem.toString());
            }
            public void onNothingSelected( AdapterView< ? > arg0 ) {
            }
        });

        final String[] labels = getResources().getStringArray(R.array.array_orientlabels);
        final String[] values = getResources().getStringArray(R.array.array_orientvalues);
        orientSpinner = (Spinner) findViewById(R.id.orientSpinner);
        ArrayAdapter< ? > orientSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.array_orientlabels,
                android.R.layout.simple_spinner_item);
        orientSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        orientSpinner.setAdapter(orientSpinnerAdapter);
        orientSpinner.setSelection(1);
        currentOrientation = orientSpinner.getSelectedItem().toString();
        orientSpinner.setOnItemSelectedListener(new OnItemSelectedListener(){
            public void onItemSelected( AdapterView< ? > arg0, View arg1, int arg2, long arg3 ) {
                Object selectedItem = orientSpinner.getSelectedItem();
                currentOrientation = selectedItem.toString();
                if (currentOrientation.equals(labels[0])) {
                    currentOrientation = values[0];
                } else {
                    currentOrientation = values[1];
                }
            }
            public void onNothingSelected( AdapterView< ? > arg0 ) {
            }
        });

        scaleSpinner = (Spinner) findViewById(R.id.scaleSpinner);
        ArrayAdapter< ? > scaleSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.array_scales,
                android.R.layout.simple_spinner_item);
        scaleSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        scaleSpinner.setAdapter(scaleSpinnerAdapter);
        scaleSpinner.setSelection(4);
        currentScale = scaleSpinner.getSelectedItem().toString();
        scaleSpinner.setOnItemSelectedListener(new OnItemSelectedListener(){
            public void onItemSelected( AdapterView< ? > arg0, View arg1, int arg2, long arg3 ) {
                Object selectedItem = scaleSpinner.getSelectedItem();
                currentScale = selectedItem.toString().replaceFirst("%", "");
            }
            public void onNothingSelected( AdapterView< ? > arg0 ) {
            }
        });

        String state = Environment.getExternalStorageState();
        boolean mExternalStorageAvailable;
        boolean mExternalStorageWriteable;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
        if (mExternalStorageAvailable && mExternalStorageWriteable) {
            File sdcardDir = Environment.getExternalStorageDirectory();// new
            hyumlDir = new File(sdcardDir.getAbsolutePath() + File.separator + "hyuml");
            if (!hyumlDir.exists()) {
                hyumlDir.mkdirs();
            }
            canSave = true;
        } else {
            openDialog("The external card is not writable, it will not be possible to use the saving functions.");
            canSave = false;
        }

    }

    public boolean onCreateOptionsMenu( Menu menu ) {
        super.onCreateOptionsMenu(menu);
        if (canSave) {
            menu.add(Menu.NONE, MENU_LOAD, 0, "Load").setIcon(android.R.drawable.ic_menu_add);
            menu.add(Menu.NONE, MENU_SAVE, 1, "Save").setIcon(android.R.drawable.ic_menu_save);
            menu.add(Menu.NONE, MENU_SAVEAS, 2, "Saveas");// .setIcon(android.R.drawable.ic_menu_set_as);
            menu.add(Menu.NONE, MENU_SAVEIMAGE, 2, "Save Image");// .setIcon(android.R.drawable.ic_menu_set_as);
        }
        menu.add(Menu.NONE, MENU_EXIT, 3, "Exit").setIcon(android.R.drawable.ic_lock_power_off);
        menu.add(Menu.NONE, MENU_ABOUT, 4, "About").setIcon(android.R.drawable.ic_menu_info_details);

        return true;
    }

    private static final int LOAD_FILE_CODE = 666;
    protected static final String LAST_SAVED_FILEKEY = "LAST_SAVED_FILEKEY";

    public boolean onMenuItemSelected( int featureId, MenuItem item ) {
        switch( item.getItemId() ) {
        case MENU_ABOUT:
            openDialog("Hyuml is created by HydroloGIS and is a GPL licensed application.");
            return true;
        case MENU_LOAD:
            Intent browseIntent = new Intent(Constants.DIRECTORYBROWSER);
            browseIntent.putExtra(Constants.INITIALPATH, hyumlDir.getAbsolutePath());
            startActivityForResult(browseIntent, LOAD_FILE_CODE);
            return true;
        case MENU_SAVE:
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String lastSavedFile = preferences.getString(LAST_SAVED_FILEKEY, "");
            if (lastSavedFile.length() == 0) {
                saveAs();
            } else {
                String script = scriptText.getText().toString();
                try {
                    FileUtilities.writeFile(script, new File(lastSavedFile));
                    Toast.makeText(HyumlActivity.this, "File saved", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(HyumlActivity.this, "An error occurred while saving the file", Toast.LENGTH_LONG).show();
                }
            }
            return true;
        case MENU_SAVEAS:
            saveAs();
            return true;
        case MENU_SAVEIMAGE:
            saveImageAs();
            return true;
        case MENU_EXIT:
            finish();
            return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    private void saveAs() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Save");
        alert.setMessage("Enter a filename to save the script to");
        final EditText input = new EditText(this);
        alert.setView(input);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
            public void onClick( DialogInterface dialog, int whichButton ) {
                Editable value = input.getText();
                File file = new File(hyumlDir + File.separator + value.toString());
                String script = scriptText.getText().toString();
                try {
                    FileUtilities.writeFile(script, file);
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    Editor editor = preferences.edit();
                    editor.putString(LAST_SAVED_FILEKEY, file.getAbsolutePath());
                    editor.commit();
                    Toast.makeText(HyumlActivity.this, "File saved", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(HyumlActivity.this, "An error occurred while saving the file", Toast.LENGTH_LONG).show();
                }
            }
        });
        alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(){
            public void onClick( DialogInterface dialog, int whichButton ) {
            }
        });
        alert.show();
    }

    private void saveImageAs() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Save Image");
        alert.setMessage("Enter a filename to save the image to");
        final EditText input = new EditText(this);
        alert.setView(input);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
            public void onClick( DialogInterface dialog, int whichButton ) {
                if (currentImage == null) {
                    openDialog("No image available yet.");
                    return;
                }
                try {
                    Editable value = input.getText();
                    File file = new File(hyumlDir + File.separator + value.toString());

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    currentImage.compress(Bitmap.CompressFormat.PNG, 80, bytes);
                    FileOutputStream fo = new FileOutputStream(file);
                    fo.write(bytes.toByteArray());

                    Toast.makeText(HyumlActivity.this, "Image saved", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(HyumlActivity.this, "An error occurred while saving the file", Toast.LENGTH_LONG).show();
                }
            }
        });
        alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(){
            public void onClick( DialogInterface dialog, int whichButton ) {
            }
        });
        alert.show();
    }
    private void openDialog( String msg ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(HyumlActivity.this);
        builder.setMessage(msg).setCancelable(false).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
            public void onClick( DialogInterface dialog, int id ) {
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
        super.onActivityResult(requestCode, resultCode, data);
        switch( requestCode ) {
        case (LOAD_FILE_CODE): {
            if (resultCode == Activity.RESULT_OK) {
                String chosenFolderToLoad = data.getStringExtra(Constants.PATH);
                if (chosenFolderToLoad != null && new File(chosenFolderToLoad).exists()) {
                    String readFile;
                    try {
                        readFile = FileUtilities.readFile(chosenFolderToLoad);
                        scriptText.setText(readFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
        }
        }
    }
}