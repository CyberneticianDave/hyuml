package eu.hydrologis.hyuml;

import java.io.File;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class HyumlPlugin extends AbstractUIPlugin {

    // The plug-in ID
    public static final String PLUGIN_ID = "eu.hydrologis.hyuml"; //$NON-NLS-1$

    // The shared instance
    private static HyumlPlugin plugin;

    /**
     * The constructor
     */
    public HyumlPlugin() {
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
     */
    public void start( BundleContext context ) throws Exception {
        super.start(context);
        plugin = this;
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
     */
    public void stop( BundleContext context ) throws Exception {
        plugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance
     *
     * @return the shared instance
     */
    public static HyumlPlugin getDefault() {
        return plugin;
    }

    /**
     * Returns an image descriptor for the image file at the given
     * plug-in relative path
     *
     * @param path the path
     * @return the image descriptor
     */
    public static ImageDescriptor getImageDescriptor( String path ) {
        return imageDescriptorFromPlugin(PLUGIN_ID, path);
    }

    public static final String LAST_CHOSEN_FOLDER = "LAST_CHOSEN_FOLDER";
    public static final String LAST_SAVEDFILE = "LAST_SAVEDFILE";

    /**
     * Utility method for file dialogs to retrieve the last folder.
     * 
     * @return the path to the last folder chosen or the home folder.
     */
    public String getLastFolderChosen() {
        IPreferenceStore store = getPreferenceStore();
        String lastFolder = store.getString(LAST_CHOSEN_FOLDER);

        if (lastFolder != null) {
            File f = new File(lastFolder);
            if (f.exists() && f.isDirectory()) {
                return lastFolder;
            }
            if (f.exists() && f.isFile()) {
                return f.getParent();
            }
        }

        return new File(System.getProperty("user.home")).getAbsolutePath();
    }

    /**
     * Utility method for file dialogs to set the last folder.
     * 
     * @param folderPath
     */
    public void setLastFolderChosen( String folderPath ) {
        IPreferenceStore store = getPreferenceStore();
        store.putValue(LAST_CHOSEN_FOLDER, folderPath);
    }

    public File getLastSavedFile() {
        IPreferenceStore store = getPreferenceStore();
        String lastFile = store.getString(LAST_SAVEDFILE);

        if (lastFile != null) {
            File f = new File(lastFile);
            if (f.exists()) {
                return f;
            }
        }

        return null;
    }

    public void setLastSavedFile( String filePath ) {
        IPreferenceStore store = getPreferenceStore();
        store.putValue(LAST_SAVEDFILE, filePath);
    }

}
