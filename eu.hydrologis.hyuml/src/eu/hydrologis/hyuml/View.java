package eu.hydrologis.hyuml;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.IProgressService;

import eu.hydrologis.hyuml.ImageUtils.Diagram;

public class View extends ViewPart implements SelectionListener {
    private static final String RENDERING = "Rendering...";
    private static final String RENDER_DIAGRAM = "Render Diagram";

    public static final String ID = "eu.hydrologis.hyuml.view";
    private static final int HEIGHT = 70;
    private Text scriptText;
    private Label imageLabel;
    private Image lastDrawnImage;
    private Button renderButton;
    private Diagram diagramType = Diagram.CLASS;
    private Button classButton;
    private Button activityButton;
    private Button usecaseButton;
    private ScrolledComposite scrolledComposite;
    private Text fileText;
    private Text htmltagText;
    private Combo orientationCombo;
    private Combo scaleCombo;
    private String[] orientValues = new String[]{"dir:lr", "dir:td"};
    private String[] orientLabels = new String[]{"left->right", "top->down"};
    private Button scrubbyButton;
    private Button yumlButton;
    private Button nospaceYumlButton;
    private Button bitbButton;

    /**
     * This is a callback that will allow us to create the viewer and initialize
     * it.
     */
    public void createPartControl( Composite parent ) {

        SashForm sashForm = new SashForm(parent, SWT.HORIZONTAL);
        sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        // script part
        Group scriptGroup = new Group(sashForm, SWT.NONE);
        scriptGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        scriptGroup.setLayout(new GridLayout(1, false));
        scriptGroup.setText("Script");

        scriptText = new Text(scriptGroup, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL | SWT.BORDER);
        scriptText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        File lastSavedFile = HyumlPlugin.getDefault().getLastSavedFile();
        if (lastSavedFile != null && lastSavedFile.exists()) {
            try {
                String readFile = FileUtilities.readFile(lastSavedFile);
                scriptText.setText(readFile);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } else {
            scriptText.setText("");
        }

        Group fileGroup = new Group(scriptGroup, SWT.NONE);
        GridData fileGroupGD = new GridData(SWT.FILL, SWT.FILL, true, false);
        fileGroupGD.heightHint = HEIGHT;
        fileGroup.setLayoutData(fileGroupGD);
        fileGroup.setLayout(new GridLayout(1, false));
        fileGroup.setText("File");

        fileText = new Text(fileGroup, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL | SWT.BORDER | SWT.READ_ONLY);
        GridData fileTextGD = new GridData(SWT.FILL, SWT.FILL, true, false);
        fileTextGD.heightHint = HEIGHT - 15;
        fileText.setLayoutData(fileTextGD);
        if (lastSavedFile != null && lastSavedFile.exists()) {
            fileText.setText(lastSavedFile.getAbsolutePath());
        }

        Group htmltagGroup = new Group(scriptGroup, SWT.NONE);
        GridData htmltagGroupGD = new GridData(SWT.FILL, SWT.FILL, true, false);
        htmltagGroupGD.heightHint = HEIGHT + 30;
        htmltagGroup.setLayoutData(htmltagGroupGD);
        htmltagGroup.setLayout(new GridLayout(2, false));
        htmltagGroup.setText("Html Tag");

        htmltagText = new Text(htmltagGroup, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL | SWT.BORDER | SWT.READ_ONLY);
        GridData htmltagTextGD = new GridData(SWT.FILL, SWT.FILL, true, false);
        htmltagTextGD.heightHint = HEIGHT + 20;
        htmltagText.setLayoutData(htmltagTextGD);

        Composite buttonComposite = new Composite(htmltagGroup, SWT.NONE);
        buttonComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
        buttonComposite.setLayout(new GridLayout(1, false));
        yumlButton = new Button(buttonComposite, SWT.RADIO);
        yumlButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        yumlButton.setText("y");
        yumlButton.setToolTipText("yuml syntax url");
        yumlButton.setSelection(true);
        nospaceYumlButton = new Button(buttonComposite, SWT.RADIO);
        nospaceYumlButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        nospaceYumlButton.setText("yns");
        nospaceYumlButton.setToolTipText("yuml with safe chars syntax");
        nospaceYumlButton.setSelection(false);
        bitbButton = new Button(buttonComposite, SWT.RADIO);
        bitbButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        bitbButton.setText("bitb");
        bitbButton.setToolTipText("Bitbucket wiki syntax");
        bitbButton.setSelection(false);

        Group imageGroup = new Group(sashForm, SWT.NONE);
        GridData imageGroupGD = new GridData(SWT.FILL, SWT.FILL, true, true);
        imageGroupGD.horizontalSpan = 2;
        imageGroup.setLayoutData(imageGroupGD);
        imageGroup.setLayout(new GridLayout(1, false));
        imageGroup.setText("Diagram");

        renderButton = new Button(imageGroup, SWT.PUSH);
        renderButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        renderButton.setText(RENDER_DIAGRAM);
        renderButton.addSelectionListener(new SelectionAdapter(){
            public void widgetSelected( SelectionEvent e ) {

                IWorkbench wb = PlatformUI.getWorkbench();
                IProgressService ps = wb.getProgressService();
                try {
                    ps.busyCursorWhile(new IRunnableWithProgress(){
                        public void run( IProgressMonitor pm ) {
                            Display.getDefault().syncExec(new Runnable(){

                                public void run() {
                                    try {
                                        renderButton.setText(RENDERING);
                                        try {
                                            String orient = getDiagramOrientation();
                                            String scale = getScale();
                                            boolean scrubby = scrubbyButton.getSelection();

                                            Image drawnImage = ImageUtils.getImage(scriptText.getText(), diagramType, orient,
                                                    scale, scrubby);
                                            imageLabel.setImage(drawnImage);

                                            ImageData imageData = drawnImage.getImageData();
                                            scrolledComposite.setMinSize(new Point(imageData.width, imageData.height));
                                            scrolledComposite.layout();

                                            if (lastDrawnImage != null) {
                                                lastDrawnImage.dispose();
                                            }
                                            lastDrawnImage = drawnImage;

                                            String htmlImgTag = getWikiStringForSelection(orient, scale, scrubby);
                                            htmltagText.setText(htmlImgTag);
                                        } finally {
                                            renderButton.setText(RENDER_DIAGRAM);
                                        }
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                    }
                                }

                            });

                        }
                    });
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        Composite typeComposite = new Composite(imageGroup, SWT.NONE);
        typeComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        typeComposite.setLayout(new GridLayout(3, true));

        classButton = new Button(typeComposite, SWT.TOGGLE);
        classButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        classButton.setText("Class");
        classButton.setData(Diagram.CLASS);
        classButton.setSelection(true);
        classButton.addSelectionListener(this);

        activityButton = new Button(typeComposite, SWT.TOGGLE);
        activityButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        activityButton.setText("Activity");
        activityButton.setData(Diagram.ACTIVITY);
        activityButton.addSelectionListener(this);

        usecaseButton = new Button(typeComposite, SWT.TOGGLE);
        usecaseButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        usecaseButton.setText("Usecase");
        usecaseButton.setData(Diagram.USECASE);
        usecaseButton.addSelectionListener(this);

        Composite orientationComposite = new Composite(imageGroup, SWT.NONE);
        orientationComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        orientationComposite.setLayout(new GridLayout(6, false));

        scrubbyButton = new Button(orientationComposite, SWT.CHECK);
        scrubbyButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        scrubbyButton.setText("Scrubby");

        Label orientationLabel = new Label(orientationComposite, SWT.NONE);
        orientationLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        orientationLabel.setText("Orientation");

        orientationCombo = new Combo(orientationComposite, SWT.DROP_DOWN | SWT.READ_ONLY);
        orientationCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
        orientationCombo.setItems(orientLabels);
        orientationCombo.select(1);

        Label dummyLabel = new Label(orientationComposite, SWT.NONE);
        GridData dummyLabelGD = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        dummyLabelGD.widthHint = 20;
        dummyLabel.setLayoutData(dummyLabelGD);

        Label scaleLabel = new Label(orientationComposite, SWT.NONE);
        scaleLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        scaleLabel.setText("Scale");

        scaleCombo = new Combo(orientationComposite, SWT.DROP_DOWN | SWT.READ_ONLY);
        scaleCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
        scaleCombo.setItems(new String[]{"10%", "25%", "50%", "75%", "100%", "125%", "150%"});
        scaleCombo.select(4);

        scrolledComposite = new ScrolledComposite(imageGroup, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
        scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        scrolledComposite.setExpandVertical(true);
        scrolledComposite.setExpandHorizontal(true);

        imageLabel = new Label(scrolledComposite, SWT.BORDER);
        imageLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        imageLabel.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

        scrolledComposite.setContent(imageLabel);
        // Point point = imageLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        // imageLabel.setSize(point);
        scrolledComposite.setMinSize(new Point(500, 500));

        sashForm.setWeights(new int[]{1, 3});

        try {
            String orientation = getDiagramOrientation();
            String scale = getScale();
            boolean scrubby = scrubbyButton.getSelection();
            String htmlImgTag = getWikiStringForSelection(orientation, scale, scrubby);
            htmltagText.setText(htmlImgTag);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private String getWikiStringForSelection( String orient, String scale, boolean scrubby ) throws Exception {
        String htmlImgTag = "";
        if (bitbButton.getSelection()) {
            htmlImgTag = ImageUtils.getHtmlImgTagSafeBitBucket(scriptText.getText(), diagramType, orient, scale, scrubby);
        } else if (nospaceYumlButton.getSelection()) {
            htmlImgTag = ImageUtils.getHtmlImgTagSafe(scriptText.getText(), diagramType, orient, scale, scrubby);
        } else {
            htmlImgTag = ImageUtils.getHtmlImgTag(scriptText.getText(), diagramType, orient, scale, scrubby);
        }
        return htmlImgTag;
    }

    private String getScale() {
        String scale = scaleCombo.getText().replaceAll("%", "");
        return scale;
    }

    private String getDiagramOrientation() {
        String orient = orientationCombo.getText();
        if (orient.equals(orientLabels[0])) {
            orient = orientValues[0];
        } else {
            orient = orientValues[1];
        }
        return orient;
    }

    public void saveScript( String path ) throws IOException {
        String script = scriptText.getText();
        File file = new File(path);
        FileUtilities.writeFile(script, file);
        fileText.setText(path);
    }

    public void saveImage( String path ) {
        ImageLoader imageLoader = new ImageLoader();
        imageLoader.data = new ImageData[]{lastDrawnImage.getImageData()};
        imageLoader.save(path, SWT.IMAGE_PNG);
    }

    public void loadScript( String path ) throws IOException {
        File scriptFile = new File(path);
        String text = FileUtilities.readFile(scriptFile);
        fileText.setText(path);
        HyumlPlugin.getDefault().setLastSavedFile(path);
        scriptText.setText(text);
    }

    /**
     * Passing the focus request to the viewer's control.
     */
    public void setFocus() {
    }

    public void widgetSelected( SelectionEvent e ) {
        Button source = (Button) e.getSource();
        diagramType = (Diagram) source.getData();
        switch( diagramType ) {
        case CLASS:
            activityButton.setSelection(false);
            usecaseButton.setSelection(false);
            break;
        case ACTIVITY:
            classButton.setSelection(false);
            usecaseButton.setSelection(false);

            break;
        case USECASE:
            classButton.setSelection(false);
            activityButton.setSelection(false);

            break;

        default:
            break;
        }
    }

    public void widgetDefaultSelected( SelectionEvent e ) {
    }

}