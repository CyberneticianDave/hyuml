/*
 * Copyright 2010 Alternate Computing Solutions Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.hydrologis.hyuml;

import java.net.URL;
import java.util.StringTokenizer;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

/**
 * Image utilities
 */
public class ImageUtils {

    private static final String YUML_ME_BASEURL = "http://yuml.me/diagram/";
    // private static final String YUML_ME_BASEURL = "http://yuml.me/diagram/scruffy/";

    public enum Diagram {
        CLASS("class"), ACTIVITY("activity"), USECASE("usecase");

        private final String value;

        Diagram( String value ) {
            this.value = value;
        }

        public String getName() {
            return value;
        }
    }

    /**
     * populates the image of the given diagram
     * @param scrubby 
     * @return 
     * 
     * @throws Exception 
     */
    public static Image getImage( String script, Diagram diagram, String orientation, String scale, boolean scrubby )
            throws Exception {
        String urlString = getUrlString(script, diagram, orientation, scale, scrubby);
        urlString = urlString.replaceAll(" ", "%20");
        URL url = new URL(urlString);

        ImageDescriptor imageDesc = ImageDescriptor.createFromURL(url);
        Image image = imageDesc.createImage();
        // BufferedImage image = ImageIO.read(url);
        return image;
    }

    public static String getHtmlImgTag( String script, Diagram diagram, String orientation, String scale, boolean scrubby )
            throws Exception {
        String urlString = getUrlString(script, diagram, orientation, scale, scrubby);
        urlString = "<img src=\"" + urlString + "\" />";
        return urlString;
    }

    public static String getHtmlImgTagSafe( String script, Diagram diagram, String orientation, String scale, boolean scrubby )
            throws Exception {
        String urlString = getUrlString(script, diagram, orientation, scale, scrubby);
        urlString = urlString.replaceAll(" ", "%20");
        urlString = urlString.replaceAll("\\|", "%7C");
        urlString = urlString.replaceAll("\\<", "%3C");
        urlString = urlString.replaceAll("\\>", "%3E");
        urlString = "<img src=\"" + urlString + "\" />";
        return urlString;
    }

    public static String getHtmlImgTagSafeBitBucket( String script, Diagram diagram, String orientation, String scale,
            boolean scrubby ) throws Exception {
        String urlString = getUrlString(script, diagram, orientation, scale, scrubby);
        urlString = urlString.replaceAll(" ", "%20");
        urlString = urlString.replaceAll("\\|", "%7C");
        urlString = urlString.replaceAll("\\<", "%3C");
        urlString = urlString.replaceAll("\\>", "%3E");
        urlString = "{{" + urlString + "|image}}";
        return urlString;
    }

    private static String getUrlString( String script, Diagram diagram, String orientation, String scale, boolean scrubby ) {
        if (script.length() == 0) {
            return "";
        }
        StringTokenizer st = new StringTokenizer(script.trim(), "\n");
        StringBuilder buffer = new StringBuilder();
        while( st.hasMoreTokens() ) {
            String trim = st.nextToken().trim();
            if (trim.length() > 0 && !trim.startsWith("#")) {
                buffer.append(trim);
                if (diagram != Diagram.ACTIVITY)
                    if (st.hasMoreTokens() && !trim.trim().endsWith(",")) {
                        buffer.append(",");
                    }
            }
        }
        StringBuilder sb = new StringBuilder();
        if (orientation != null) {
            sb.append(orientation);
        }
        if (scale != null) {
            if (orientation != null) {
                sb.append(";");
            }
            sb.append("scale:").append(scale);
        }
        if (sb.length() > 0) {
            sb.append("/");
        }

        String scruffy = "";
        if (scrubby) {
            scruffy = "scruffy;";
        }
        String urlString = YUML_ME_BASEURL + scruffy + sb.toString() + diagram.getName() + "/" + buffer.toString();
        urlString = urlString + ".png";
        return urlString;
    }

}
