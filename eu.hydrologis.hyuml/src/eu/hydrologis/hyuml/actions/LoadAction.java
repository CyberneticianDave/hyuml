package eu.hydrologis.hyuml.actions;

import java.io.IOException;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IViewActionDelegate;
import org.eclipse.ui.IViewPart;

import eu.hydrologis.hyuml.HyumlPlugin;
import eu.hydrologis.hyuml.View;

public class LoadAction implements IViewActionDelegate {

    private View view;

    @Override
    public void run( IAction action ) {

        FileDialog fileDialog = new FileDialog(view.getSite().getShell(), SWT.OPEN);
        String lastFolderChosen = HyumlPlugin.getDefault().getLastFolderChosen();
        fileDialog.setFilterPath(lastFolderChosen);
        String path = fileDialog.open();

        if (path != null && path.length() >= 1) {
            try {
                view.loadScript(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void selectionChanged( IAction action, ISelection selection ) {
    }

    @Override
    public void init( IViewPart view ) {
        this.view = (View) view;
    }

}
