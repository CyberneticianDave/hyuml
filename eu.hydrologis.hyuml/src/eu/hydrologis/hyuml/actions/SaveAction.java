package eu.hydrologis.hyuml.actions;

import java.io.File;
import java.io.IOException;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IViewActionDelegate;
import org.eclipse.ui.IViewPart;

import eu.hydrologis.hyuml.HyumlPlugin;
import eu.hydrologis.hyuml.View;

public class SaveAction implements IViewActionDelegate {

    private View view;

    @Override
    public void run( IAction action ) {

        File lastSavedFile = HyumlPlugin.getDefault().getLastSavedFile();
        try {
            if (lastSavedFile != null) {
                view.saveScript(lastSavedFile.getAbsolutePath());
            } else {
                MessageDialog.openWarning(view.getSite().getShell(), "Warning",
                        "Have no file to save, use the save as dialog instead.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void selectionChanged( IAction action, ISelection selection ) {
    }

    @Override
    public void init( IViewPart view ) {
        this.view = (View) view;
    }

}
